# TODO
## IMPORTANT
- whole tree insertion as change to timestamp


## LESS IMPORTANT
- option to show edge length always
- come up with a way to switch from differences view to update view
- add visual hint for node removal
- make tabs highlight when page switches
- right click nodes for change submission
- radial layout
- proportional triangles
- user auth
- separate timestamp and tree route in API

# DONE
- ~~implement the changes like they are supposed to be~~ 
- ~~implement sliding window~~
- ~~support florests~~
- ~~make GET /tree/:id return newest timestamp for that tree~~
- ~~redirect to latest timestamp when See Tree btn is clicked on view page~~
- ~~remodel homepage~~
- ~~host in openshift~~
- ~~see branch length on hover~~
- ~~query time tests~~
- ~~fix page freeze on bad timestamp in differences~~
- ~~error views for dyta~~
- ~~fix blank page on non existing tree~~
- ~~change branch length/height to partials~~
- ~~favicon~~
- ~~optimize changes code (1 connection, 1 transaction)~~
- ~~fix page freeze on bad timestamp on view/:id/timestamp/:ts~~
- ~~fix updates for nodes other than leafs~~
- ~~refactor changes code~~
- ~~fix root bugs on update/remove~~
- ~~return 400 if ti >= tf on differences~~
- ~~remove unecessary dyta logging~~
- ~~fix removals for nodes other than leafs~~
- ~~forms only show on latest timestamp~~
