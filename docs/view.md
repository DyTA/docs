# How to view your tree

The following picture shows the view page.
In this page you can view one of your trees or the differences between two timestamps in one of your trees.

![view_page](_static/view_page.png)

## Viewing a single tree

To view a single tree write it's identifier in the first input box and click the `See tree` button. When you do this DyTA will redirect your browser to that tree's detail page where you can see your tree displayed in full detail.

## Viewing differences between two timestamps

To view differences between two timestamps you will use the second form in this page.
Write your tree's identifier in the first box of this form and the timestamps that you wish to see in the second and third box. After this click the `See differences` button.
Your browser will be redirected to the differences page where you can see the differences highlighted in color between those two timestamps.