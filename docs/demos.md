# Demonstrations

Here are some demonstrations on how to use the various functionalities of DyTA:

1. Structure of the Web Application

	<iframe width="560" height="315" src="https://www.youtube.com/embed/-fgopmFi3Ig" frameborder="0" allowfullscreen></iframe>

2. Functionalities of the Dendrogram View

	<iframe width="560" height="315" src="https://www.youtube.com/embed/MMDd0euDmc4" frameborder="0" allowfullscreen></iframe>

3. Functionalities of the Graph-like View

	<iframe width="560" height="315" src="https://www.youtube.com/embed/HTCO4KelFgw" frameborder="0" allowfullscreen></iframe>

4. Insertion and visualization of differences for Graph-like trees

	<iframe width="560" height="315" src="https://www.youtube.com/embed/11OOplREKVk" frameborder="0" allowfullscreen></iframe>

5. Insertion and visualization of differences for Dendrogram trees

	<iframe width="560" height="315" src="https://www.youtube.com/embed/4A6RUYI14VU" frameborder="0" allowfullscreen></iframe>
