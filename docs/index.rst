Welcome to DyTA's documentation!
================================

.. toctree::
   :maxdepth: 2
   
   navigate.md
   insert.md
   view.md
   detail.md
   update.md
   differences.md
   demos.md


Introduction
============

DyTA is a web application designed to help you visualize and analyze the evolution of phylogenetic trees over time.

It does this by allowing you to submit changes to your trees, using our custom format or the Newick format, 
and recording them in what we call a timestamp.
This timestamp represents a moment in the evolution of a tree where some changes were submitted to it.
Using the data recorded in the several timestamps of a tree's evolution DyTA will highlight with different colors
the changes you have made. 

You can access DyTA using the following link: http://cloud131.ncg.ingrid.pt/home

DyTA Team
-----------------

- `João Correia`_, ADEETC, ISEL, Instituto Politécnico de Lisboa
- `Paulo Fonseca`_, ADEETC, ISEL, Instituto Politécnico de Lisboa
- `Cátia Vaz`_, INESC-ID / ADEETC, ISEL, Instituto Politécnico de Lisboa

.. _João Correia: https://github.com/Dude29
.. _Paulo Fonseca: https://github.com/semvento
.. _Cátia Vaz: http://pwp.net.ipl.pt/cc.isel/cvaz/
