# How to navigate DyTA

![navbar](_static/navbar.jpg)

The picture above shows DyTA's navigation bar. This navigation bar will always be present at the top of the current page so you can always access it. By clicking on the buttons in the navbar you can go to the different pages in the website.

The first button will take you to the homepage.
The second one will take you to the insert page, where you can insert your trees.
The last button will take you to the view page where you can view a single tree or the differences between two timestamps.