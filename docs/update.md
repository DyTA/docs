# How to submit changes to your tree

This picture shows the bottom part of the detail page where you can submit changes to your trees using DyTA's custom format or the Newick format.

![update](_static/update.png)

The blue bar(`a` in the picture) is a slider that you can click and drag to show the various timestamps of the tree you are viewing. When you click or drag the slider to one of the blue circles DyTA will load your tree's state at that timestamp. This way you can quickly see the different states of your tree over time.

When you click the submit buttons to perform changes to your tree DyTA will generate a JSON object representing the updates you specified and write it's representation to the change box(letter `g` in the picture).

When you have finished indicating the changes click the `Send tree's new timestamp` button to perform the updates. When you do this DyTA sends this generated object to it's API. In there the changes will be processed and validated and if there are no errors during the process your browser will be redirected to the newly created timestamp so you can see the most recent state of your tree. If any error occurs during this process a message box will show up in the bottom right corner of your screen.

``` eval_rst
.. important:: 
	Any changes you submit are always relative to the latest timestamp of the tree even if you navigated to another one using the slider.
```

## Submit changes using Newick

To submit your changes in Newick you must click the blue button(`b` in the picture) to alternate between the update methods.
The form you use to submit Newick changes looks like this:

![update_nwk](_static/newick_update.png)

Here you can paste the Newick representation of your tree in its new timestamp. To change the update format click the blue button again.

## DyTA's custom format

In order to submit individual node changes DyTA provides a custom format that you can edit manually. It is represented in JSON and describes a list of changes, grouped by type, to be submitted in a timestamp.

```json
{
  "add":[
      {"generatedID":-1,"parentVid":10,"vertexData":"6","branchLength":"2.7"},
      {"generatedID":-2,"parentVid":-1,"vertexData":"9","branchLength":"1.33"},
  ],
  "update":[
      {"vid":"6","vertexData":"12"}
  ],
  "remove":[
      {"vid":"3"}, {"vid":"4"}
  ]
}
```

This list of changes can have the following types of changes:
- Addition, represented by the JSON property `add`
- Removal, represented by the JSON property `remove`
- Update, represented by the JSON property `update`

If any of these properties is not present in the change format sent to the API then no change of that type is performed. For example:
```json
{
  "remove":[ {"vid":"3"}, {"vid":"4"} ]
}
```
This object represents a list of changes that includes the removal of nodes `3` and `4` but no updates or additions to any other nodes.

Each of these properties(`add`, `remove` and `update`) contains a set of JSON objects that represent the operation of the corresponding type to perform.

The objects representing a node addition must have the following properties:

- `parentVid` identifier of the parent node of the new node
- `vertexData` content of the new node
- `branchLength` length of the edge linking the new node to it's parent
- `generatedID` numeric value that identifies this new node. This value is used to refer to this new node before it exists.

The objects representing a node removal must have the following property:

- `vid` identifier of the node to remove

The objects representing a node update must have the following properties:

- `vid` identifier of the node to change
- `vertexData` new node content

## Remove a node

To remove an existing node from a tree you can use the provided form(letter `e` in the picture). All you need to do is type the identifier of the node you wish to remove in the input box and click `Remove vertex`. 

## Change the value of a node

To change the contents of an already existing node you can use the update form(letter `d` in the picture) and insert the node identifier in the first input box. Then insert the new content of the node and click `Update vertex`.

## Insert a new node

To insert a new node in the tree you can use the add form(letter `c` in the picture).
You must specify the parent node and branch length. The new node's content is not required. There are two ways to specify the parent node's identifier. You can hover your mouse pointer over an existing node to find out it's identifier and insert that in the first box. However if you wish to insert a node and it's children in the same timestamp this method will not work because the parent node does not exist in the current timestamp yet. 

To solve this issue DyTA allows you to insert "fake" identifiers in the parent id input box so you can refer to a newly inserted node. Here is an example:

First we insert the parent node as the child of an existing node and we DyTA generates this change object:
```json
{
  "add":[ 
      {"generatedID":-1,"parentVid":10,"vertexData":"6","branchLength":"2.7"}
  ]
}
```
Notice the `generatedID` property. This is the "fake" identifier you will use to refer the newly created node.
To add child nodes to this new node in the same timestamp all you have to do is insert the value of `generatedID` in the parent id input box and DyTA will know you intent to add new nodes to this newly created node. The resulting change object looks like this:

```json
{
  "add":[ 
      {"generatedID":-1,"parentVid":10,"vertexData":"6","branchLength":"2.7"},
	  {"generatedID":-2,"parentVid":-1,"vertexData":"11","branchLength":"0.9"},
  ]
}
```

## Manually insert your changes

You can manually write your own changes to the tree if you wish. To do this you can write your changes in the manual changes form(letter `f` in the picture) and then click the `Add my changes` button. The only thing you need to guarantee is that the changes you wrote conform to DyTA's custom format, otherwise your changes will be considered invalid and will not be applied.
