# How to insert your own trees

The following picture shows the insert page, which is where you can insert your trees.

![insert_example](_static/insert_example.png)

The radio buttons let you choose the way your tree will be displayed. Currently, DyTA only supports two ways to display your trees: Dendrogram and as a graph using a Force Directed Layout.
The dendrogram option will display a tree with a named root where only the leaf nodes have values.
The graph-like option is used for both single trees and forests of trees. In these trees all the nodes have values. 
Due to the nature of the tree types, the accepted Newick formats for each visualization are the following:

- ***popular format***, for Dendrogram visualization;
- ***distances and all names***, for Graph-like visualization;

The next input box is where you can put the contents on your tree described in Newick format. This format must be valid Newick otherwise your tree might not be correctly displayed. If you wish to insert a forest of trees instead then you can do so by writing one tree per line separated by the character ` ; `. Like this:
```
(7:1,8:1)9;
(1);
(2);
(3);
(4);
(5);
(6);
```

``` eval_rst
.. important:: 
	If you chose the Dendrogam type then you must make sure your Newick input describes a tree in which only the leaf nodes have values, otherwise your tree might not be rendered correctly. You must also ensure that in the input for graph-like trees all nodes have values.
```

The other input boxes are used for naming and for providing an optional description to your tree.

When you click the `Submit my tree` button DyTA will redirect your browser to the details page where you can view your tree and all it's details. This process may take some time depending on the size of your tree.

## Example trees

Dendrogram tree:
```
(((((0:1.5,3:1.5):0.5,6:2.0):0.33333325,1:2.3333333):0.104166746,(4:1.0,5:1.0):1.4375):0.14583325,2:2.5833333);
```

Graph-like tree:
```
(((2:4)1:3)4:4,5:2,3:5)6;
```

Forest of trees:
```
(7:1,8:1)9;
(1);
(2);
(3);
(4);
(5);
(6);
```