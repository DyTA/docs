# How to view differences in a tree

The picture below shows the difference view which is where two timestamps of a tree are compared side-by-side and their differences highlighted with color.

![update](_static/differences_coloring.png)

Much like the tree detail page, this page also has a slider on the bottom that allows you to quickly change between timestamps in the tree you're viewing. This slider works slightly differently from the one in the detail page because it allows you to move the left circle to define the timestamp shown on the left side of the view.

Atop this view there are also new controls as can be seen in this next picture:

![update](_static/diff_bar.png)

The checkbox shown in this picture changes the color palette that is used to highlight the changes in the tree to a color palette that is more suited for colorblind users.

## Representation of each type of change

Each type of change that can occur in a tree is represented in a different way.

- In the normal color palette, new nodes are painted in an orange color(letter `a` in the first picture of this page)
- In the normal color palette, when a node gets linked to a node it wasn't linked before gets that new link painted in a light yellow color(letter `b` in the first picture of this page)
- Removals of nodes are represented by a dashed line as is shown in picture(letter `c` in the first picture of this page)
- Updates on nodes values are represented by the removal of the old node and the insertion of a new one, therefore are painted like a new node with an orange color

## Sliding-window

This page provides an alternate mode to view the changes called the ***sliding window*** mode.
It can be acessed by clicking the blue `Change view mode` button.
By clicking this button the view will no longer be split in two and the slider at the bottom of the page will change to only allow one timestamp to be shown. In this mode this slider is called the ***sliding window***.

This mode highlights with the same color scheme as before the changes that occurred in a specific timestamp pointed by the ***sliding window***. This allows you to view what changes were made to the tree in that specific timestamp of it's evolution.