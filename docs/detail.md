# The tree detail page

![view_page](_static/detail_top.png)

This picture shows the top part of the detail page where you have access to some of the funcionalities that this page offers.

The name of your tree and it's description are highlighted by the letter `a` and `b` respectively.

If you happen to forget the identifier of your tree then you can click its name to pop up a baloon with the identifier. To dismiss this baloon click the name again.

![id_baloon](_static/id_baloon.png)

## Functionalities

This page has a set of functionalities that you can use to explore and analyze your tree. 
It allows you to search for a specific node, export your tree in Newick format, zoom and pan the view and modify the size and height of the tree branches.
Most importantly it allows you to switch rapidly between the several timestamps your tree might have with a slider located at the bottom of the page.

### Find a specific node in a tree

To find a specific node in your tree you must write it's name in the search box(letter `c` in the picture) and click the blue button with the magnifying glass next to it.
If the node you are looking for exists then your screen will be centered on it and the path from the root to the target node will be expanded.
If your node can't be found then an error message will appear in the bottom right corner of your screen.

### Export your tree as Newick

To export your tree at the current timestamp you are viewing all you have to do is click on the blue button with a downward arrow symbol(letter `d` in the picture) and a window will show up in the middle of your screen with the Newick representation.
To close this window you just need to click outside of it or on the cross in the top right corner or in the `Close` button located at the bottom right. 
If you need to quickly copy the Newick representation to your clipboard, so you can paste it somewhere else, then all you have to do it click the `Copy` button located at the top or bottom of this window.

### Change the dimensions of the tree

If you wish to change the displayed length of the branches in the tree or the separation between them then all you have to do is click the `+` and `-` buttons(letters `e` and `f` in the picture).

## Tree display section

This section is where your tree will be displayed with the format you chose when you inserted it. It will look very differently depending on the chosen type at the moment of creation. If you chose the `Dendrogram` option then your tree will be displayed as a dendrogram. If you chose the `Graph-like` option then your tree will be rendered with a force directed layout. Regardless of the option you chose the edges of the tree will always be proportional to the value that was specified when the tree was first inserted.
This way you can have an idea of how the tree really looks like even if you move the view around.

Both of these display modes have certain shared functionalities.
They both allow you to move the view around by clicking and dragging with the mouse pointer. You can also zoom in and out by using the mouse wheel or, if you have a laptop, your touchpad.
When you click on a node or branch the view will be centered on it and it will be expanded or collapsed.
If you just click once then only one level of the branch will be affected. If you press `ctrl` (`cmd` for mac users) and click then the entire branch below that node will be affected.

In addition to this, if you hover your mouse pointer over a node it's ID will be displayed after a while. This also works for the edges connecting the nodes, but in this case it shows their length.

### Dendrogram

![display](_static/dendrogram.png)

This picture shows the dendrogram view.
The triangles you see in the picture are representations of collapsed branches and contain several nodes.
The values you see on the side of the nodes are their names. The only exception to this is if a branch is collapsed(triangle) and in this case the value on the side of the triangle is the number of nodes that branch contains.

### Graph-like

![display](_static/graphlike.png)

This picture shows a tree rendered with the `Graph-like` view. This display mode has the aditional feature of allowing you to move the entire tree around by pulling one of the nodes. To do this you just need to click on top of a node and drag the mouse pointer to where you want the tree to move.
In this view the collapsed branches are represented in a lighter color than the uncollapsed branches and the number of nodes it contains are represented with a number inside the circle.
